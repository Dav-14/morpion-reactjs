import React from "react";
import Square from "./Square";
import './Board.css';


const morpionWidth = 3;

export default class Board extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      squares: Array(morpionWidth ** 2).fill(null),
      winner: null,
    };
  }

  isOver() {
    //TODO: implement for I*J and X align
    //For 3*3 grid
    //ROW
    let index;
    for (index = 0; index < morpionWidth; index++) {
      let row = this.state.squares.slice(
        index * morpionWidth,
        (index + 1) * morpionWidth
      );
      if (row[0] != null && row[0] == row[1] && row[1] == row[2]) {
        return row[0];
      }
    }
    //COL
    let squareLign = this.state.squares;
    for (index = 0; index < morpionWidth; index++) {
      if (
        squareLign[index] != null &&
        squareLign[index] == squareLign[3 + index] &&
        squareLign[6 + index] == squareLign[3 + index]
      ) {
        return squareLign[index];
      }
    }
    //
    if (
      squareLign[4] != null &&
      ((squareLign[0] == squareLign[4] && squareLign[4] == squareLign[8]) ||
        (squareLign[2] == squareLign[4] && squareLign[4] == squareLign[6]))
    ) {
      return squareLign[4];
    }
  }

  getSquare(i) {
    return this.state.squares[i];
  }

  renderSquare(i) {
    return (
      <Square
        value={this.getSquare(i)}
        key={i}
        onClick={() => {
          let winner = this.isOver();
          if (winner == null) {
            this.state.squares[i] = this.props.player;
            this.setState({
              squares: this.state.squares,
            });
            this.props.nextPlayer(this.state);
          } else {
            this.setState({
              winner: winner,
            });
          }
        }}
      />
    );
  }

  render() {
    let items = [];

    for (let i = 0; i < morpionWidth; i++) {
      let squares = [];

      for (let j = 0; j < morpionWidth; j++) {
        squares.push(this.renderSquare(j + i * morpionWidth));
      }

      let div = (
        <div
          className="board-row"
          style={{
            display: "inline-flex",
          }}
        >
          {squares}
        </div>
      );

      items.push(div);
      items.push(<br />);
    }

    let winner = this.isOver();

    return (
      <div className="board">
        <div className="stateBoard">
          <h1>Joueur {this.props.player} :</h1>
          {items}
        </div>
        {winner != null ? (
          <div>
            <h3>The winner is {winner}</h3>
          </div>
        ) : null}
      </div>
    );
  }
}

