import React from "react";
import Board from "./Board";

export default class Game extends React.Component{

    constructor(props){
        super(props);
        this.state = {
            players: ["X","O"],
            currentPlayerIndex: 0, 
        }
    }
    nextPlayer(){
        if (this.state.currentPlayerIndex + 1 >= this.state.players.length){
            this.setState({
                currentPlayerIndex: 0
            })
        }
        else{
            this.setState({
                currentPlayerIndex: this.state.currentPlayerIndex + 1,
            })
        }
    }

    render(){
        return (
            <Board player={this.state.players[this.state.currentPlayerIndex]} nextPlayer={()=>this.nextPlayer()} />
        );
    }
}
