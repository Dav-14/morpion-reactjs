import React from "react";

export default function Square(props){
    return (
        <button style={{ width: '30px', height: '30px', textAlign: 'center', }} 
                className="square"
                onClick={()=>{
                    if (props.value == null){
                        props.onClick();
                    }
                }}>
            {props.value}
        </button> 
    ); 
}